package az.ingress.demosecurity.service;

import az.ingress.demosecurity.dto.JwtCredentials;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SecurityService {

    private final ModelMapper modelMapper = new ModelMapper();

    public JwtCredentials getCurrentJwtCredentials() {
        var securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication())
                .map(authentication -> modelMapper.map(authentication.getPrincipal(), JwtCredentials.class))
                .orElseThrow(RuntimeException::new);
    }

    public Long getId() {
        return getCurrentJwtCredentials().getId();
    }

    public boolean checkIsNameStartsWith() {
        return getCurrentJwtCredentials().getSub().startsWith("T");
    }

}
