package az.ingress.demosecurity.controller;

import az.ingress.demosecurity.service.SecurityService;
import java.security.Principal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/security")
@RequiredArgsConstructor
@Slf4j
public class Controller {

    private final SecurityService securityService;

    @GetMapping("/test")
    public ResponseDto get() {
        return new ResponseDto("Hello World");
    }

    @GetMapping("/login")
    @PreAuthorize("@securityService.checkIsNameStartsWith()")
    public ResponseDto login(Principal principal) {
        return new ResponseDto("Welcome to the site, " + principal.getName());
    }

    @GetMapping("/user")
    public ResponseEntity<ResponseDto> user(Principal principal) {
        log.info("User id is {}", securityService.getId());
        return ResponseEntity
                .ok()
                .header("goodbye", "Good bye from Baku")
                .body(new ResponseDto("Welcome to the site " + principal.getName()));
    }

    @GetMapping("/admin")
    public ResponseDto admin() {
        return new ResponseDto("Welcome to the site, admin");
    }

    @PostMapping("/post")
    public ResponseDto post(@RequestBody ResponseDto dto) {
        return dto;
    }
}
